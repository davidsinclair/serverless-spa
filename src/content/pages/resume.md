# David Sinclair
- [sikhote.com]("https://davidsinclair.io")
- [mail@davidsinclair.io]("mailto:mail@davidsinclair.io")
- [+1 858 480 1781]("tel:+18584801781")

## Profile
I am a front end website developer with a passion to learn and contribute. I have recent experience coding responsive websites and web apps in the latest standards and technology. I enjoy collaborating with teammates and clients alike to build complete solutions. Looking forward, I strive to continue learning in an ever-changing field, building intuitive applications, and collaborating with passionate and friendly people.

## Knowledge
- Adobe Photoshop & Illustrator
- AWS Lambda & API Gateway
- Angular
- Backbone
- Bootstrap
- CSS, Sass, & Less
- Docker
- Git
- JavaScript, ES6, & jQuery
- Linux
- Node
- NPM
- PHP
- React
- Redux
- Serverless Framework
- Webpack

## Education
- Optimizely Platform Certified, 2015
- U.C. San Diego Extension—Graphic Design Certified, 2011
- U.C. San Diego—B.A. Political Science, 2008

## Experience

### Contract JavaScript Engineer at [Sony Electronics]("http://www.sony.com/en_us/SCA")
#### 2016–Present · Rancho Bernardo, CA

### Front End Engineer at [Pathway Genomics]("http://pathway.com")
#### 2015–2016 · San Diego, CA
Led and contributed to a variety of engineering projects that affected both front and back end web applications. Specialized in building JavaScript-based prototypes of the OME health app.
- Built and designed a highly successful interactive iPad web demo of OME, a health app, for the company to demonstrate at various IBM events. Demo gained public interest and generated outside investments. Backbone JS was used as a framework for managing routes, Underscore for rendering templates, and code bundling with Browserify.
- Built and designed a working prototype of OME health app in the form of a responsive web app. The prototype was a major success in helping the API team work out bugs, displaying functionality to upper management, allowing a smoother API hand-off to the iOS app developers, and allowing team leaders to adjust engineering efforts to what investors were most interested in. The prototype utilized real data, had an authentication piece, and incorporated geo-location. The prototype was built using ES6, React, Flux, Sass styling, and Webpack for bundling. The web application was responsive and targeted all devices.
- Solved issues and contributed to the OME health app team. Developed and documented Node API endpoints for the OME app, which connected to a RabbitMQ service for authentication. Fixed Objective-C and Java security and encryption issues the iOS app and QA teams encountered.
- Contributed to the general engineering team by creating APIs based on json:api specifications, developing new Angular JS pages, and modernizing frequently used tools.

### Senior Web Developer at [Elevated]("http://elevated.com")
#### 2012–2015 · Carlsbad, CA
Led and contributed to web development projects, mainly using CSS, HTML, JavaScript and PHP coding languages. Creating, theming, and maintaining content management systems, including Drupal, Magento, Movable Type, Wordpress, and Zend Framework. Managing multiple projects while utilizing contract designers and developers. Meeting with clients to explore new projects as well as discuss project progress and options. Configuring domains, internal and external web servers, maintaining the company network, on and offsite data storage, and all IT needs. Designing and developing responsive emails for MailChimp distribution.
- Magento development experience in installing and customizing Magento extensions for clients, customizing and installing themes, and creating new storefronts from start to finish.
- Created an online wine shop using a custom Wordpress theme and the Shopp plugin
- Developed the company website, including creating a custom Wordpress theme, LESS stylesheets with a two-stage responsive design, and CRM integration with Podio.
- Setup and maintenance of a linux-based file and web server. Data storage utilized a local Drobo device and Google Drive cloud storage for data loss prevention and quick access. Server was setup as a Virtual Machine under Mac OS X and made accessible locally or through SFTP.
- Improved sales efficiency by programming all lead generation forms to automatically populate the team's CRM, including advanced spam prevention.
- Aided in CRO projects with HTML, CSS, and JavaScript expertise for executing multivariate tests.

### Web Developer at [Travel To Go]("http://traveltogo.com")
#### San Diego, CA · 2012
Helped design and develop web projects in HTML5, LESS CSS3, and JavaScript/jQuery. Designed and ordered print work for client distribution. Created and coded weekly marketing emails. Organized inventory for white-label print supplies.
- Streamlined the production process for white-label marketing materials.
- Designed and developed a popular website feature into a standalone responsive website for market-testing purposes.
- Redesigned and developed the company homepage in a fully responsive theme using the ASP.NET framework. Process included the creation of a custom jQuery plugin that allowed a responsive sliding-banner system.

### Graphic Designer at [DD Studio]("http://ddstudio.com")
#### Carlsbad, CA · 2011
Helped design and develop interface for EcoATM kiosks using Flash, Illustrator, Photoshop, and XML. Created exterior artwork for EcoATM kiosks. Helped design interface for a Rain Bird sprinkler system. Designed label graphics for Rain Bird sprinkler system. Designed promotional materials for events and internal documents. Updated labels and graphics for various projects. Created presentations and participated in client meetings. Designed and prepared files for print.

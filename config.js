const name = 'davidsinclair';

module.exports.domain = () => `${name}.io`;
module.exports.bucket = () => name;
module.exports.service = () => name;
